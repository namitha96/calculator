package com.company;

public abstract class Calculator {
    public abstract int add(int n, int num1[]);

    public abstract int subtract(int n, int num1[]);
}
