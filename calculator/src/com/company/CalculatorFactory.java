package com.company;

public class CalculatorFactory {
    public static  Calculator calculatorfactory(int choice)
    {
        if(choice==1)
        {
            return new NormalCalculator();

        }
        else if(choice==2)
        {
            return new MagicCalculator();
        }
        else return null;

    }

}
