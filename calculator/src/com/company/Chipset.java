package com.company;

public class Chipset implements InterfaceOfChipset {
    public int add(int n, int num[]) {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum = sum + num[i];
        }
        return sum;
    }

    public int subtract(int n, int num[]) {
        int subtract = num[0];
        for (int i = 1; i < n; i++) {
            subtract = subtract - num[i];
        }
        return subtract;
    }
}