package com.company;

import com.company.Chipset;

public class MagicCalculator extends Calculator {
    private InterfaceOfChipset chipset = null;

    public MagicCalculator() {
        chipset = new Chipset();
    }

    public int add(int n, int num[]) {
        return chipset.subtract(n, num);
    }

    public int subtract(int n, int num[]) {
        return chipset.add(n, num);
    }
}



