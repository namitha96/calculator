package com.company;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int value, ch, n, i, choice;
        Calculator calc = null;

        System.out.println("1) Normal Calculator\n2) Magic Calculator\n");
        System.out.println("Enter the choice");
        choice = sc.nextInt();
        calc=CalculatorFactory.calculatorfactory(choice);

        System.out.println("Enter how many numbers");
        n = sc.nextInt();
        int a[] = new int[n];
        System.out.println("Enter the numbers");
        for (i = 0; i < n; i++) {
            a[i] = sc.nextInt();

        }
        System.out.println("1) Addition\n2) Subtraction\n");
        do

        {
            System.out.println("Enter the choice");
            ch = sc.nextInt();
            switch (ch) {
                case 1:
                    System.out.println("NormalCalculator Add");
                    value = calc.add(n, a);
                    System.out.println("value=" + value);
                    break;
                case 2:
                    System.out.println("NormalCalculator Subtract");
                    value = calc.subtract(n, a);
                    System.out.println("value=" + value);
                    break;

                default:
                    System.out.println("An Invalid Choice!!!\n");
            }

        } while (ch != 3);


    }
}
