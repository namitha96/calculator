package com.company;


public class NormalCalculator extends Calculator

{
    private InterfaceOfChipset chipset = null;

    public NormalCalculator() {
        chipset = new Chipset();
    }


    public int add(int n, int num[]) {
        return chipset.add(n, num);
    }

    public int subtract(int n, int num[]) {
        return chipset.subtract(n, num);
    }
}
